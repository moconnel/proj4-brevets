import arrow
import nose
import acp_times.py

def test_open_time():
    assert open_time(200, 200, "2017-01-01 08:00") == "2017-01-01T13:53:00.00+00:00"
    assert open_time(15, 200, "2017-01-01 08:00") == "2017-01-01T08:26:00.00+00:00"
    assert open_time(350, 600, "2017-01-01 08:00") == "2017-01-01T18:34:00.00+00:00"

def test_close_time():
    assert close_time(600, 600, "2017-01-01 08:00") == "2017-01-03T00:00:00.00+00:00"
    assert close_time(450, 600, "2017-01-01 08:00") == "2017-01-02T14:00:00.00+00:00"
